package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Product;

import java.util.Objects;

public class GetProductResponse {
    private String name;
    private Integer price;
    private String unit;

    public GetProductResponse(Product product) {
        Objects.requireNonNull(product);

        this.name = product.getName();
        this.price = product.getPrice();
        this.unit = product.getUnit();
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}
