package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	@PostMapping("/products")
	public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest request){
		Product product = new Product(request.getName(), request.getPrice(), request.getUnit());
		Product saveProduct = productRepository.save(product);
		URI uri = URI.create("http://localhost/api/products/" + saveProduct.getId());
		return ResponseEntity.created(uri).build();
	}
	
	@GetMapping("/products/{productId}")
	public ResponseEntity getProduct(@PathVariable Long productId){
		Optional<Product> product = productRepository.findById(productId);
		if(!product.isPresent()){
			return ResponseEntity.notFound().build();
		}
		GetProductResponse response = new GetProductResponse(product.get());
		return ResponseEntity.ok(response);
	}
}
